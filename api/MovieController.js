const MovieService = require('./MovieService')
const Validator = require('./Validator')
const MovieSchema = require('./entity/Movie').schema

async function create(movie) {
  const validatedData = await Validator.validate(movie, MovieSchema)
  const result = await MovieService.create(movie)

  return result
}

async function get(uuid) {
  const result = await MovieService.get(uuid)

  return result
}

async function getAll(query) {
  const result = await MovieService.getAll(query)

  return result
}

module.exports = {
  create,
  get,
  getAll
}
