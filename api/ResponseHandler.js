function success(response, data = {}, { code = 200, headers}) {
  const responseObject = {
    success: true,
    data
  }
  if (headers) {
    Object.entries(headers)
      .forEach(([header, value]) => response.setHeader(header, value));
  }

  return response.status(code).json(responseObject)
}

function fail(response, error, {code = 400}) {
  const responseObject = {
    success: false,
    data: {
      message: error.message
    }
  }

  return response.status(code).json(responseObject)
}

module.exports = {
  success,
  fail
}
