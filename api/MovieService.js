const db = {}
const _ = require('lodash')

async function create (movieData) {
  const movie = db[movieData.id] = movieData
  return movie
}

async function get(uuid) {
  const movie = db[uuid];

  if (!movie) {
    throw new Error('Movie not found.');
  }

  return movie
}

async function getAll(query) {
  const movies = Object.values(db)

  if (!movies || movies.length === 0) {
    throw new Error('Movies not found.');
  }

  return movies
    .filter(movie => matchesQuery(movie, query))
}

/**
 * Maps through the query objects properties and returns
 * a boolean if the condition matches.
 *
 * Then check if every booleans in the mapped array is truthy.
 */
function matchesQuery(movie, query) {
  const queryProperties = Object.keys(query)

  return queryProperties
    .map(property => {
      switch (property) {
        case 'title':
          return _.includes(query.title, movie.title)
        case 'minStars':
          return movie['stars'] >= query[property]
        case 'genre':
          return _.includes(query.genre, movie.genre);
        case 'minLength':
          return query.minLength < movie.length
        case 'maxLength':
          return query.maxLength > movie.length;
        case 'starring':
          return _.isEmpty(_.difference(query.starring, movie.actors))
      }
    })
    .every((item) => item)
}

module.exports = {
  create,
  get,
  getAll
}
