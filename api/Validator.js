const Joi = require('joi');

function validate (body, schema) {
  const options = {
    allowUnknown: true,
    abortEarly: false
  }
  const { error, value } = Joi.validate(body, schema, options);

  if (error) {
    throw new Error(error.message)
  }

  return value;
}

module.exports = {
  validate
}
