const express = require('express')
const bodyParser = require('body-parser');
const ResponseHandler = require('./ResponseHandler');
const MovieController = require('./MovieController')

const PORT = 3000;
const app = express()
app.use(bodyParser.json());

app.get('/movies', async (req, res) => {
  try {
    const movies = await MovieController.getAll(req.query)

    return ResponseHandler.success(res, movies, {});
  } catch (error) {

    return ResponseHandler.fail(res, error, {
      code: 404
    });
  }
})

app.get('/movies/:id', async (req, res) => {
  try {
    const movie = await MovieController.get(req.params.id)

    return ResponseHandler.success(res, movie, {});
  } catch (error) {

    return ResponseHandler.fail(res, error, {
      code: 404
    });
  }
})

app.post('/movies', async (req, res) => {
  try {
    const createdMovie = await MovieController.create(req.body)
    const location = `${req.hostname}:${PORT}${req.path}${createdMovie.id}`

    return ResponseHandler.success(res, createdMovie, {
      code: 201,
      headers: {
        'Location': location
      }
    });
  } catch (error) {
    return ResponseHandler.fail(res, error, {});
  }
})

app.listen(PORT, () => console.log('Example app listening on port 3000!'))
