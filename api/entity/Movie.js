const Joi = require('joi');
const VALID_GENRES = [
    'ACTION',
    'COMEDY',
    'DRAMA',
    'DOCUMENTARY',
    'HORROR'
]

const schema = Joi.object().keys({
    id: Joi.string().required(),
    title: Joi.string().required(),
    genre: Joi.string().valid(VALID_GENRES).allow(null),
    actors: Joi.array().items(Joi.string().required()).required(),
    length: Joi.any().required(),
    stars: Joi.number().min(0).max(5)
})

module.exports = {
    schema
}