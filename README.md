# Assignment

## How run the project

### Via node
Hopefully you'll have nvm installed. Otherwise install latest node version.

If you have nvm, run:

`$ nvm install `

To start the application run:

`$ npm install `

`$ npm start `


You can now use the API via curl or postman or you own web-app,
by making requests to `http://localhost:3000/movies`


### Via Docker
Navigate to the projects rootfolder, where the Dockerfile is, and run :

`$ docker build -t homework/node-app . `

This will create a Docker image of the project.
To start the project, run:

`$ docker run -p 3001:3000 -d homework/node-app`

You can now use the API via curl or postman or you own web-app,
by making requests to `http://localhost:3001/movies`
